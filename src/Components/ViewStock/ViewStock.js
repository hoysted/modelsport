import React, { Component } from 'react';
import StockItem from '../StockItem/StockItem';
import Sidebar from '../RightSidebar/RightSidebar';

class ViewStock extends Component {

    // store data
    state = {
        Stock: [
            { id:1, productName: 'Baja Buggy', Brand: 'HPI', Category: 'cars', Price: '599.99', Image: 'http://placekitten.com/70/70', stockQty: 8},
            { id:2, productName: 'HPI Savage', Brand: 'HPI', Category: 'cars', Price: '359.99', Image: 'http://placekitten.com/70/70', stockQty: 5},
            { id:3, productName: 'MCD Plane', Brand: 'MCD Racing', Category: 'Plane', Price: '199.99', Image: 'http://placekitten.com/70/70', stockQty: 2},
            { id:4, productName: 'TT-01', Brand: 'Tamiya', Category: 'cars', Price: '287.99', Image: 'http://placekitten.com/70/70', stockQty: 8},
            { id:5, productName: 'Savage Flux', Brand: 'Traxxas', Category: 'cars', Price: '359.99', Image: 'http://placekitten.com/70/70', stockQty: 5},
            { id:6, productName: 'Losi Speed T', Brand: 'LRP', Category: 'cars', Price: '19.99', Image: 'http://placekitten.com/70/70', stockQty: 2},
            { id:7, productName: 'DTM Electric 1/5', Brand: 'Carson', Category: 'cars', Price: '549.99', Image: 'http://placekitten.com/70/70', stockQty: 8},
            { id:8, productName: 'HPI Savage', Brand: 'HPI', Category: 'cars', Price: '35.99', Image: 'http://placekitten.com/70/70', stockQty: 5},
            { id:9, productName: 'BMW Gas 1/5', Brand: 'Carson', Category: 'cars', Price: '54.99', Image: 'http://placekitten.com/70/70', stockQty: 2},
            { id:10, productName: 'MCD Buggy', Brand: 'MCD Racing', Category: 'cars', Price: '67.99', Image: 'http://placekitten.com/70/70', stockQty: 2}            
        ]
    }
    elements = [];

    // filters and be able to 
    // delete products and 
    // re route to add products
    purchaseStockItem = (index, name, price) => {
        const action = window.confirm(`Confirm purchase of ${name} for £${price}.`);
        if(action === true) {
            // grab individual item
            const currentStockItem = {...this.state.Stock[index]};
            if(currentStockItem.stockQty === 0) {
                alert('sorry we have no ' + name + ' in stock.');
                return;
            }
            currentStockItem.stockQty = currentStockItem.stockQty - 1;
            //grab rest of stocklist 
            const stockList = [...this.state.Stock];
            //push update into main list and then state
            stockList[index] = currentStockItem;
            this.setState({ Stock: stockList }); 
        } else {
            return
        }
    }

    render() {
        return (
            <div>
            <div className="stock-wrapper">
                {this.state.Stock.map((item, index) => {
                    return  <div key={item.id}>
                            <StockItem 
                                id={item.id} 
                                productName={item.productName} 
                                brand={item.Brand} 
                                category={item.Category} 
                                price={item.Price} 
                                image={item.Image} 
                                stockQty={item.stockQty} 
                                purchaseFnc={() => this.purchaseStockItem(index, item.productName, item.Price)}/>
                        </div>
                })}
            </div>
            <Sidebar />
            </div>
        )
    }
}

export default ViewStock;