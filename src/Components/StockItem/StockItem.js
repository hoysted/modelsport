import React from 'react';

const stockItem = (props) => {
    
        return (
            <div className="product-item-row">
                <div className="column"><img alt={props.productName} src={props.image}/></div>
                <div className="column"><p>{props.brand} : {props.productName}</p></div>
                
                <div className="column">£{props.price}</div>
                <div className="column">Stock: {props.stockQty}</div>
                <div className="column">Category: {props.category}</div>
                <div className={props.stockQty > 0 ? 'available' : 'not-available'}><button onClick={props.purchaseFnc}>Purchase</button></div>
            </div>
        )
    
}

export default stockItem;