import React, { Component } from 'react'
import './Navigation.css';
import { Link } from "react-router-dom";

class Nav extends Component {
  render() {
    return ( 
        <div className="navigation">    
          <Link to="/">Home Page</Link>
          <Link to="/add-product">Add a New Product</Link>
        </div>
    );
  }
}

export default Nav