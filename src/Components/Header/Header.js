import React, { Component } from 'react';

class Header extends Component {
    constructor(props) {
        super(props);

        this.state = {
            userName: 'Luke Hoysted'
        }
    }

    render() {
        return(
            <header className="App-header">
                <h1>Welcome {this.state.userName}</h1>
            </header>
        )
    }
}

export default Header;