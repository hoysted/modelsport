import React, { Component } from 'react';
import './App.css';
import Header from './Components/Header/Header';
import Nav from './Components/Header/Navigation';
import ViewStock from './Components/ViewStock/ViewStock';
import Footer from './Components/Footer/Footer';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import AddItem from './Components/AddItem/AddItem';

// app where you can browse a list of model cars and purchase one via a modal on click

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Router>
          <div class="main-container">
            <Nav />
            <Switch>
              <Route path="/add-product" component={AddItem} />
              <Route path="/" component={ViewStock} />
            </Switch>
          </div>
        </Router>
        <Footer />
      </div>
    );
  }
}

export default App;
